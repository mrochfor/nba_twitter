\documentclass{article}

% if you need to pass options to natbib, use, e.g.:
%     \PassOptionsToPackage{numbers, compress}{natbib}
% before loading neurips_2021

% ready for submission
%\usepackage{neurips_2021}

% to compile a preprint version, e.g., for submission to arXiv, add add the
% [preprint] option:
     \usepackage[preprint]{neurips_2021}

% to compile a camera-ready version, add the [final] option, e.g.:
%     \usepackage[final]{neurips_2021}

% to avoid loading the natbib package, add option nonatbib:
\usepackage[nonatbib]{neurips_2021}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{xcolor}         % colors
%\graphicspath{{figures/}}
\title{An NLP Approach to Sports Betting: Leveraging NBA Twitter Data}

% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
%
% Using \And between authors leaves it to LaTeX to determine where to break the
% lines. Using \AND forces a line break at that point. So, if LaTeX puts 3 of 4
% authors names on the first line, and the last on the second line, try using
% \AND instead of \And before the third author name.

\author{
  Matt Rochford \\
  UC Santa Cruz \\
  \texttt{mrochfor@ucsc.edu} 
  % examples of more authors
  % \And
  % Coauthor \\
  % Affiliation \\
  % Address \\
  % \texttt{email} \\
  % \AND
  % Coauthor \\
  % Affiliation \\
  % Address \\
  % \texttt{email} \\
  % \And
  % Coauthor \\
  % Affiliation \\
  % Address \\
  % \texttt{email} \\
  % \And
  % Coauthor \\
  % Affiliation \\
  % Address \\
  % \texttt{email} \\
}

\begin{document}

\maketitle

\begin{abstract}
Predicting the outcomes of sporting events is a lucrative problem that has attracted the attention of statisticians and computer scientists. Traditional approaches have centered on using statistical data relating to the prior performance of each team in a game and various machine learning algorithms, such as SVM or FC-NN. An alternative approach to use crowd sentiment as an alternative information source has also seen success. In this work, we present a pipeline for the automatic gathering of relevant Tweets for NBA games, a new publicly available dataset of those tweets, and a trained CNN sentiment detector specific to basketball related tweets. 
\end{abstract}

\section{Introduction}

% Paragraph about sports betting 
Sports betting is a rapidly growing industry projected to reach a value of \$180 billion by 2028, with an annual growth rate just below 10\% \cite{GlobalSp24:online}. It involves a sportsbook setting the odds for the results of future sporting events (a 'line'). Bettors then try to select the winning side of each bet in order to make a profit off of the sportsbook. Contributing to the rapid growth of the sports betting market is the introduction of online sportsbooks in the past decade such as Fanduel or Draft Kings, leading to a hugely growing player base. With so much money at stake, sportbooks and bettors alike have turned to advanced statistical algorithms to help make decisions when placing bets.
\\
\\
% Paragraph about this work
This work focuses on betting regarding the National Basketball Association (NBA), including moneyline (ML) bets and against the spread (ATS) bets, the two most common bet types for this sport. ML bets consist of choosing an outright winner for the game, with plus odds for the underdog and minus odds for the favorite. ATS bets consist of a spread determined by the sportsbook, that is then added to the underdogs final score to determine the winner of the bet. Take for example the game Clippers vs Cavaliers, with the ML odds set at +190 for the Clippers and -230 for the Cavaliers. If you place a wager on the Clippers ML and win you will have your original bet amount returned plus winnings of 1.9 times the original amount. If you place a wager on the Cavaliers ML and win you will have your original bet amount returned plus winnings of the original amount divided by 2.3. Similarly, for this game the spread is set at +5.5 for the Clippers, with standard odds of -110 for both sides of the spread. In the ATS scenario if the Clippers lost the game by 3, they would lose the ML bet but win the ATS bet because the +5.5 points added to their score would make them the adjusted winners.

This work has three key components. First, an automated framework will be developed that checks the sportsbook's lines each morning, then pulls data from Twitter related to each bet. This framework will then be used to assemble a dataset consisting of games, their betting lines, and publicly available tweets related to these games; this dataset will then be provided for any kind of use. Lastly, tweets will be labeled according to positive sentiment, negative sentiment, or unrelated, and these labeled tweets will then be used to train a CNN sentiment detector.

\section{Background and Related Work}
% Paragraph about sabermetrics
The study of sports statistics can be traced back to the field of Sabermetrics. Sabermetrics is the empirical analysis of the game of baseball through statistics and was established through Bill James' 1977 paper \textit{Baseball Abstracts}. Although it was slow to gain widespread acceptance, the Oakland Athletic's success in 2002 using sabermetric principles and an extremely analytically based approach led to popularity of the method. The

% Paragraph about statistical approaches 
Some of the first works in statistical modeling for sports betting were from British researchers Forrest and Goddard \cite{forrest2005odds,forrest2008soccer}. Their work focused on employing statistical models towards soccer bets. Since these papers, a community has formed regarding the application of statistical models towards sports betting, with the primary focus being on European soccer. With the increases in consumer availability of high performance GPUs and open source machine learning (ML) libraries during the 2010s, focus in this field shifted towards the use of ML models. A majority of this work has used neural network models with input data consisting of various statistics and trends relating to the teams in each bet. \cite{hubavcek2019exploiting} serves as a good historical summary of approaches and also a target benchmark model to consider as SOTA.
\\
\\
An approach to use text based data in a sports betting model has be seen before in \cite{hong2010wisdom,sinha2013predicting,schumaker2016predicting}. \cite{hong2010wisdom} uses sentiment analysis pulled from various websites to estimate an overall crowd sentiment for NFL games, they show that this information has value on predictions for NFL point spread betting. \cite{sinha2013predicting} also focuses on text data for the NFL, this time pulled from Twitter, and uses Tweet volume relating to each team in a bet to make bet predictions. \cite{schumaker2016predicting} similarly uses Twitter data, but applies sentiment analysis on each tweet to make predictions on English Premier League bets (soccer).

\section{Approach}

An overview of our approach can be seen in Figure \ref{fig:overview}. The whole pipeline will be run by the cron scheduler at the start of each day. The first script will use the 'odds-api' to get today's lines for each NBA game from each sportsbook we are considering using. Then for each game we will gather the required information to build the features. First, we will gather a predetermined set of statistics from Basketball Reference using Beautiful Soup. Next, all tweets from within the target time frame related to each team in the bet  will be gathered. A sentiment analysis will be performed on each tweet and the text based features will be calculated. The text based and numeric features will then be fused together before being input into a neural network model. The output of the network will be a prediction on which way to bet the line.

\begin{figure} [h]
    \centering
    \includegraphics[scale=0.45]{figures/model_overview.png}
    \caption{Proposed processing pipeline}
    \label{fig:overview}
\end{figure}


Tweets will be collected using an academic research license and the Tweepy python library. A search will be performed to gather all tweets relating to a certain keyword related to the bet, in a given time frame. Time frames ranging from 1 hour to 6 hours will be tested to determine the optimal time window of Tweets to consider. Sentiment analysis will be performed using the nltk Python library module 'SentimentIntensityAnalyzer'. Calculating the text-based features will include metrics like overall sentiment, number of tweets, sentiment ratio, sentiment change from the average, etc. Lastly, some specific Twitter users who routinely post regarding NBA bets can be used as additional features. 
\\
\\
The feature fusion process will test different attenuation schemes for the numeric and text based features. The attenuation schemes to be tested will include 100/0, 75/25, 50/50, and 0/100 where each number represents numerical and text based features respectively. Since the prediction is likely to be more influenced by the numerical data, more attenuation schemes that favor that data will be tested. An attenuation that includes only the text based data will be done to include a full analysis of our approach. Lastly, a standard feed forward neural network will be used to perform predictions using Keras and Tensorflow libraries in Python. The main research focus of this work will be on the derivation of the text-based features from the Twitter data, and their subsequent fusing with the numerical data and their affect on model accuracy.

\section{Implementation}

\section{Results}
The first output of this work was to develop an automated pipeline for collecting tweets each day and storing the resulting data. This

\section{Conclusion}
This work showed the difficulty of attempting a problem without a well-defined dataset. The added work of assembling a new dataset and then the subsequent labeling 

Additionally, the short timeline of ~8 weeks proved challenging. Original plans 

All code is available on Gitlab at %ENTER GITLAB LINK%

% 4. References Cited
\newpage%\newsection{References}
\renewcommand\refname{References}
\bibliography{references}
\bibliographystyle{IEEEtran}

\end{document}
