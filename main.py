## CSE 244B Research Project - Winter 2022
## An NLP Approach to Sports Betting - Leveraging NBA Twitter Data
## Matt Rochford

# References
# https://medium.com/analytics-vidhya/intro-to-scraping-basketball-reference-data-8adcaa79664a


# General purpose
from dotenv import load_dotenv
import os
import pandas as pd


# For odds api
import requests
import json
import ciso8601
from datetime import datetime, timezone, date

# For twitter api
import tweepy

# # For sports reference api
# from sportsipy.nba.boxscore import Boxscore

# # Import Modules
# from urllib.request import urlopen
# from bs4 import BeautifulSoup

def main():

    # Load API credentials
    load_dotenv()

    # Get todays odds
    odds = get_odds()

    # Get todays games
    games = get_games(odds)

    # Get twitter data for each game
    twitter_data = get_twitter_data(games)

def get_twitter_data(games,noOfTweet=100):
    """
    This function returns NBA odds for the given day. Code is adapted from:
    https://the-odds-api.com/liveapi/guides/v4/samples.html

    Input:
    games - list of tuples of todays games

    Output:
    odds_json - json data for today's games (dictionary)
    """

    # We only need read only access so use OAuth2

    # Get credentials from env file
    api_key = os.environ.get('TWITTER_API_KEY')
    api_key_secret = os.environ.get('TWITTER_API_KEY_SECRET')
    access_token = os.environ.get('TWITTER_ACCESS_TOKEN')
    access_token_secret = os.environ.get('TWITTER_ACCESS_TOKEN_SECRET')

    
    # Create authentication object
    auth = tweepy.OAuthHandler(api_key, api_key_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)

    # Initialize array to store data
    dataframe_list = []

    # Search for tweets for each game
    for game in games:

        # Get team names 
        home_team = game[0].split()[1]
        away_team = game[1].split()[1]

        # Search for tweets
        home_tweets = api.search_tweets(q=home_team,lang='en',result_type='recent',count=noOfTweet)
        away_tweets = api.search_tweets(q=away_team,lang='en',result_type='recent',count=noOfTweet)

        print('{} tweets found for the {}'.format(len(home_tweets),home_team))
        print('{} tweets found for the {}'.format(len(away_tweets),away_team))

        print('Starting labeling process for {} tweets'.format(home_team))
        for tweet in home_tweets:
            
            print(tweet.text)

            # Store sample in dataframe
            dataframe_list.append([home_team,away_team,tweet.text,tweet,None,None])

        print('Starting labeling process for {} tweets'.format(away_team))
        for tweet in away_tweets:
            
            print(tweet.text)

            # Store sample in dataframe
            dataframe_list.append([away_team,home_team,tweet.text,tweet,None,None])

    cols = ['Team','Opponent','Tweet','Tweet Metadata','Label','Gametime']
    df = pd.DataFrame(dataframe_list,columns=cols)

    print('Tweet dataframe collected with size:'+str(df.shape))

    # Save to pickle file
    df.to_csv('df.csv')
    df.to_pickle('df.pkl')


def label_data(df):

    for index,record in df.iterrows():
        label = input('Enter label (0,1,2): ')

def get_odds():
    """
    This function returns NBA odds for the given day. Code is adapted from:
    https://the-odds-api.com/liveapi/guides/v4/samples.html

    Input:
    none

    Output:
    odds_json - json data for today's games (dictionary)
    """

    try:
        
        with open('data.json','r',encoding='utf-8') as f:
            odds_json = json.load(f)
            
            # Get date of first event
            game_date = odds_json[0]['commence_time'] 
            game_date = ciso8601.parse_datetime(game_date) # Convert str to datetime object
            game_date = game_date.replace(tzinfo=timezone.utc).astimezone(tz=None) # Convert to local time

            # Check if date is for today
            today = date.today()

            if game_date.date() >= today:
                print("Today's odds already loaded - retrieved from json file")
            else:
                print("Odds out of date - reloading new odds")
                raise Exception()

    except:
        
        print("Preparing API call to odds-api")

        # Define parameters
        SPORT = 'basketball_nba'
        REGIONS = 'us'
        MARKETS = 'spreads' # Options: h2h,spreads,totals,outrights(no nba)
        ODDS_FORMAT = 'decimal'
        DATE_FORMAT = 'iso'

        # Load API key
        API_KEY = os.environ.get('ODDS_API_KEY')

        # Get list of odds for sport
        odds_response = requests.get(
            f'https://api.the-odds-api.com/v4/sports/{SPORT}/odds',
            params={
                'api_key': API_KEY,
                'regions': REGIONS,
                'markets': MARKETS,
                'oddsFormat': ODDS_FORMAT,
                'dateFormat': DATE_FORMAT,
            }
        )

        if odds_response.status_code != 200:
            print(f'Failed to get odds: status_code {odds_response.status_code}, response body {odds_response.text}')

        else:
            odds_json = odds_response.json()
            print(f'Odds retrieved for {len(odds_json)} games:')
            #print(odds_json)

            # Check the usage quota
            print('Remaining requests', odds_response.headers['x-requests-remaining'])
            print('Used requests', odds_response.headers['x-requests-used'])

            # Save to json file to save on requests
            with open('data.json', 'w', encoding='utf-8') as f:
                json.dump(odds_json, f, ensure_ascii=False, indent=4)
                print('Odds data successfully loaded to JSON file')

    return odds_json

def get_games(odds):
    """
    This function gets todays games from the odds json

    Inputs:
    odds - dictionary of todays odds for each game

    Outputs:
    games - list of tuples containing each team playing
    """

    # Get list of games
    games = []
    for odd in odds:
        games.append((odd['home_team'],odd['away_team']))

    return games

def get_player_data():
    url = "https://www.basketball-reference.com/players/m/moranja01.html"

    # collect HTML data
    html = urlopen(url)
            
    # create beautiful soup object from HTML
    soup = BeautifulSoup(html, features="lxml") 

    # use getText()to extract the headers into a list
    headers = [th.getText() for th in soup.findAll('tr', limit=2)[1].findAll('th')]

    # get rows from table
    rows = soup.findAll('tr')[2:]
    rows_data = [[td.getText() for td in rows[i].findAll('td')]
                        for i in range(len(rows))]
    # if you print row_data here you'll see an empty row
    # so, remove the empty row
    rows_data.pop(20)

    print(rows_data)

if __name__ == '__main__':
    main()