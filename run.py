# CSE244B - HW1 - Fake News Detection
# Matt Rochford
# Did you receive any help whatsoever from anyone in solving this assignment? No
# Did you give any help whatsoever to anyone in solving this assignment? No

# Import Libraries
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import pandas as pd
import numpy as np
import nltk
#from matplotlib import pyplot as plt
from keras.preprocessing.text import text_to_word_sequence                   
# from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers.core import Dense,Dropout,Activation,Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras import layers
import csv
from collections import Counter
from nltk.corpus import stopwords

# Download NLTK resources
nltk.download('punkt')
nltk.download('stopwords')
# os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
# import tensorflow as tf

def run(train_file, valid_file, test_file):
    """ 
    This is the main function that performs the fake news detection training and testing

    Inputs:
    train_file: string - the path to the training file 
    valid_file: string - the path to the validation file
    test_file: string - the path to the testing file

    Outputs:
    output text file updated
    """

    # Define model parameters
    embedding_dimension = 300
    sentence_dimension = 40

    # Load data
    headers = ['label','statement','subject','speaker','speaker_job','state','party','context']
    train_df = pd.read_csv(train_file,sep='\t',names=headers,quoting=csv.QUOTE_NONE, error_bad_lines=False)
    valid_df = pd.read_csv(valid_file,sep='\t',names=headers,quoting=csv.QUOTE_NONE, error_bad_lines=False)
    test_df = pd.read_csv(test_file,sep='\t',names=headers[1:],quoting=csv.QUOTE_NONE, error_bad_lines=False)

    # Get shape of dataframes
    print()
    print('Training data has shape: {}'.format(train_df.shape))
    print('Validation data has shape: {}'.format(valid_df.shape))
    print('Testing data has shape: {}'.format(test_df.shape))
    print()

    # Analyze training data
    analyze_train_data(train_df)

    # Get training & validation samples & labels
    train_data,train_label = prepare_samples_and_labels(train_df,embedding_dimension,sentence_dimension)
    valid_data,valid_label = prepare_samples_and_labels(valid_df,embedding_dimension,sentence_dimension)
    print('Processed training data has shape: {}'.format(train_data.shape))

    # Get training & validation samples & labels
    train_data_subject,train_label = prepare_samples_and_labels_subject(train_df,embedding_dimension,10)
    valid_data_subject,valid_label = prepare_samples_and_labels_subject(valid_df,embedding_dimension,10)
    print('Processed training data has shape: {}'.format(train_data.shape))

    # Initialize CNN model
    try:
        model_cnn = load_model(model_path)
    except:
        model_cnn = initialize_model_1D(train_data[0])
        model_cnn_s = initialize_model_1D(train_data_subject[0])
    
    # Train model
    model_cnn.fit(train_data,train_label,epochs=5,validation_data=(valid_data,valid_label),batch_size=16)
    #model_cnn_s.fit(train_data_subject,train_label,epochs=5,validation_data=(valid_data_subject,valid_label),batch_size=16)

    # Concatenate features
    #encoding = party_encoding(train_df)
    #train_data_c = concatenate_features(train_data,train_df,model_cnn,encoding)
    #ttvalid_data_c = concatenate_features(valid_data,valid_df,model_cnn,encoding)

    # Fuse features
    train_data_c = fuse_features(train_data,train_data_subject,model_cnn,model_cnn_s)
    valid_data_c = fuse_features(valid_data,valid_data_subject,model_cnn,model_cnn_s)  

    # Initialize FC model
    try:
        model_fc = load_model(model_path)
    except:
        model_fc = initialize_model_FC(train_data_c[0,:])

    # Train model
    #model_fc.fit(train_data_c,train_label,epochs=15,validation_data=(valid_data_c,valid_label),batch_size=16)

    # Test model
    test_data = generate_sentence_matrices(test_df['statement'].values,embedding_dimension,sentence_dimension)
    test_data_s = generate_sentence_matrices(test_df['subject'].values,embedding_dimension,10)

    test_model(model_cnn,test_data)
    #test_model_fused(model_cnn,model_cnn_s,model_fc,test_data,test_data_s)

def analyze_train_data(train_df):

    # Create array to store statement sizes
    sentence_lengths = []

    # Get length of each statment
    for index,row in train_df.iterrows():
        sentence_list = nltk.word_tokenize(row['statement'])
        sentence_lengths.append(len(sentence_list))
        if sentence_lengths[-1] == 475:
            print(row['statement'])

    # Create array to store statement sizes
    description_lengths = []

    # Get length of each statment
    for index,row in train_df.iterrows():
        description_list = nltk.word_tokenize(row['subject'])
        description_lengths.append(len(description_list))
        if description_lengths[-1] == 475:
            print(row['statement'])

    # Analyze length data
    print('The max statment length is: {}'.format(max(sentence_lengths)))
    print('The min statment length is: {}'.format(min(sentence_lengths)))
    print('The average statment length is: {}'.format(sum(sentence_lengths)/len(sentence_lengths)))
    print()
    # Analyze length data
    print('The max statment length is: {}'.format(max(description_lengths)))
    print('The min statment length is: {}'.format(min(description_lengths)))
    print('The average statment length is: {}'.format(sum(description_lengths)/len(description_lengths)))
    print()

    ###################### AVERAGES
    # statement = 20.17
    # subject = 3.34

    # # Make histogram of length data
    # plt.hist(sentence_lengths,100)
    # plt.show()

    # # Check longest and shortest lengths
    # sentence_lengths.sort()
    # print('The shortest lengths are: {}'.format(sentence_lengths[:5]))
    # print('The longest lengths are: {}'.format(sentence_lengths[-5:]))

    # Check each field
    #print(train_df.party.unique())
    #print(len(train_df.party.unique()))

    parties = train_df.party.values
    print(Counter(parties))

def initialize_model_2D(sample):
    
    # Get individual sample shape for input layer
    [m,n] = sample.shape

    model = Sequential()
    model.add(Convolution2D(input_shape=(m,n,1),filters=4,kernel_size=2,strides=1,activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2),strides=(2,2)))
    model.add(Convolution2D(filters=8,kernel_size=2,strides=1,activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2),strides=(2,2)))
    model.add(Convolution2D(filters=16,kernel_size=2,strides=1,activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2),strides=(2,2)))
    model.add(Flatten())
    model.add(Dense(50, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(10, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(6, activation='softmax'))

    model.compile(optimizer='adam',loss='categorical_crossentropy',metrics=['accuracy'])
    #model.build(sample.shape)
    print(model.summary())

    return model

def initialize_model_1D(sample):
    
    # Get individual sample shape for input layer
    [m,n] = sample.shape

    model = Sequential()
    model.add(layers.Conv1D(128, 5, activation='relu',input_shape=sample.shape))
    model.add(layers.GlobalMaxPooling1D())
    model.add(Dense(24, activation='relu'))
    model.add(Dense(6, activation='softmax'))

    model.compile(optimizer='adam',loss='categorical_crossentropy',metrics=['accuracy'])

    print(model.summary())

    return model


def prepare_samples_and_labels(df,embedding_dimension,sentence_dimension):
    """ 
    This function converts a dataframe into usable numpy arrays for model training

    Inputs:
    df - dataset stored as pandas dataframe

    Outputs:
    samples - numpy array of sentence matrices for each sample in dataset (3D array)
    labels - numpy array of truth vectors (6 columns, as many rows as input label array)
    """

    # Split data into samples and labels
    samples = df['statement'].values
    labels = df['label'].values

    # Prepare samples and labels
    samples = generate_sentence_matrices(samples,embedding_dimension,sentence_dimension)
    labels = generate_labels(labels)

    return samples,labels

def prepare_samples_and_labels_subject(df,embedding_dimension,sentence_dimension):
    """ 
    This function converts a dataframe into usable numpy arrays for model training

    Inputs:
    df - dataset stored as pandas dataframe

    Outputs:
    samples - numpy array of sentence matrices for each sample in dataset (3D array)
    labels - numpy array of truth vectors (6 columns, as many rows as input label array)
    """

    # Split data into samples and labels
    samples = df['subject'].values
    labels = df['label'].values

    # Prepare samples and labels
    samples = generate_sentence_matrices(samples,embedding_dimension,10)
    labels = generate_labels(labels)

    return samples,labels


def generate_sentence_matrices(train_data,embedding_dimension,sentence_dimension):
    """ 
    This function builds a sentence matrix where each row is the vector of a word in a sentence

    Inputs:
    train_data - numpy array of word embeddings
    sentence_dimension - number of words to include in sentence matrix (zero pad if less, cut off if more)
    embedding_dimension - dimension for individual word vectors

    Outputs:
    train_data_array - numpy array of sentence matrices for each sample in dataset (3D array)
    """

    stop = stopwords.words('english')

    # Get pretrained embeddings from file (using GloVe)
    embeddings = get_pretrained_embeddings('../glove.6B.{}d.txt'.format(embedding_dimension))

    # Initialize numpy array to store word embeddings
    train_data_array = np.zeros([len(train_data),sentence_dimension,embedding_dimension],dtype=float)
    
    # Loop through label dataframe and update numpy array
    for i in range(len(train_data)):

        sentence = train_data[i]

        # Tokenize sentence
        sentence = text_to_word_sequence(sentence)
        #sentence = [word for word in sentence if word not in stop]
        for j in range(sentence_dimension):

            # Get word from sentence list
            try:
                word = sentence[j]

                try:
                    vector = embeddings[word]

                except: # Word is not in embedding dictionary
                    vector = np.random.rand(embedding_dimension)

            except: # Sentence is shorter than specified dimension                
                vector = np.zeros(embedding_dimension) # Zero padding

            train_data_array[i,j,:] = vector

    return train_data_array


def generate_labels(labels):
    """ 
    This function builds a usable label array out of the truth labels in the dataset

    Inputs:
    labels - numpy array of truth labels ('pants-fire','half-true',etc.) (1 column)

    Outputs:
    label_array - numpy array of truth vectors (6 columns, as many rows as input label array)
    """

    # Create dictionary for label mapping
    indices = {'pants-fire':0,'false':1,'barely-true':2,'half-true':3,'mostly-true':4,'true':5}

    # Initialize numpy array to store labels
    label_array = np.zeros([len(labels),6],dtype=int)
    
    # Loop through label dataframe and update numpy array
    i = 0
    for label in labels:
        label_array[i,indices[label]] = 1
        i += 1

    return label_array


def get_pretrained_embeddings(path):
    """
    This function converts a GloVe pretrained embeddings file into a python dictionary
    Code is taken from keras source code found here: https://keras.io/examples/nlp/pretrained_word_embeddings/
    
    Inputs:
    path - path to pretrained embeddings file

    Outputs:
    embeddings_index - dictionary of word embeddings
    """

    # Initialize array to store embeddings
    embeddings_index = {}

    # Loop through each line in file
    with open(path) as f:
        for line in f:
            word, coefs = line.split(maxsplit=1)
            coefs = np.fromstring(coefs, "f", sep=" ")
            embeddings_index[word] = coefs

    return embeddings_index

def party_encoding(train_df):
    
    # Make one hot encoding for party affiliations
    i = 0
    encoding = {}
    for party in train_df.party.unique():
        encoding[party] = i
        i += 1

    encoding = {'republican':0,'democrat':1,'other':2}

    return encoding

def concatenate_features(data,df,model,encoding):

    predictions = model.predict(data)
    concatenated_data = np.zeros([len(df),len(encoding)+6])

    for i in range(len(df)):

        # Insert CNN predictions
        concatenated_data[i,:6] = predictions[i]

        # Get encoding value
        party = df.at[i,'party']
        try:
            index = encoding[party]
            
        except:
            index = 2

        concatenated_data[i,6+index] = 1

    print(concatenated_data.shape)
    return concatenated_data

def fuse_features(data,data_s,model,model_s):

    predictions = model.predict(data)
    predictions_s = model_s.predict(data_s)
    
    fused_data = np.hstack((predictions,predictions_s))
    
    print("Shape of fused data"+str(fused_data.shape))

    return fused_data

def initialize_model_FC(sample):

    model = Sequential()
    model.add(Dense(40, input_dim=len(sample), activation='relu'))
    model.add(Dense(10, activation='relu'))
    model.add(Dense(6, activation='sigmoid'))

    model.compile(optimizer='adam',loss='categorical_crossentropy',metrics=['accuracy'])
    #model.build(sample.shape)
    print(model.summary())

    return model
        

def test_model(model,test_data):
    
    truth_values = {0:'pants-fire',1:'false',2:'barely-true',3:'half-true',4:'mostly-true',5:'true'}

    predictions = model.predict(test_data)

    data = ''

    for prediction in predictions:
        prediction = np.argmax(prediction)

        data += str(truth_values[prediction])+'\n'


    file = open("predictions.txt","w")
    file.write(data[:-1])
    # Close output file
    file.close()

    print('Output file generated')

def test_model_fused(model,model_s,model_fc,test_data,test_data_s):
    
    truth_values = {0:'pants-fire',1:'false',2:'barely-true',3:'half-true',4:'mostly-true',5:'true'}

    predictions_text = model.predict(test_data)
    predictions_s = model_s.predict(test_data_s)

    #fused_data = np.hstack((predictions_text,predictions_s))

    #predictions = model_fc.predict(fused_data)
    predictions = np.add(predictions_text,predictions_s*0.5)

    data = ''

    for prediction in predictions:
        prediction = np.argmax(prediction)

        data += str(truth_values[prediction])+'\n'


    file = open("predictions.txt","w")
    file.write(data[:-1])
    # Close output file
    file.close()

    print('Output file generated')


if __name__ == '__main__':

    # Define file paths
    train_file = '../train.tsv'
    valid_file = '../valid.tsv'
    test_file = '../test.tsv'

    # Call main function
    run(train_file, valid_file, test_file)